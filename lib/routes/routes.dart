import 'package:flutter/material.dart';
import 'package:separate_api/app/presentation/pages/home.dart';
import 'package:separate_api/app/presentation/views/checkout_details.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home': (context) => const HomePage(),
  'checkout': (context) => const CheckoutDetailsPage(),
};

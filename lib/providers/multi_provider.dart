import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:separate_api/app/presentation/provider/app_controller.dart';

class MultiProviderWidget extends StatelessWidget {
  const MultiProviderWidget({Key? key, required this.child}) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CatalogCartAndCheckout>(
          create: (context) => CatalogCartAndCheckout()..init(),
        )
      ],
      child: child,
    );
  }
}

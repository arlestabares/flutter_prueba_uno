import 'package:separate_api/app/data/datasources/remote/services.dart';

class Repository {
  final service = Services();

  Future<dynamic> getProducts() async {
    return service.getProducts();
  }

  Future<dynamic> getCoupon(String code) async {
    return service.getCoupon(code);
  }
}

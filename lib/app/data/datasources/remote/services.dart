import 'dart:convert';

import 'package:api/api.dart';

class Services {
  final api = Api();

 Future<dynamic> getProducts() async {
    return {"result": jsonDecode((await api.getProducts()).body)["result"]};
  }

 Future<dynamic> getCoupon(String code) async {
    return {"result": jsonDecode((await api.getCoupon(code)).body)["result"]};
  }
}

import 'package:flutter/material.dart';
import 'package:separate_api/app/data/models/coupon.dart';
import 'package:separate_api/app/data/models/product.dart';
import 'package:separate_api/app/data/repositories/repository.dart';

class CatalogCartAndCheckout extends ChangeNotifier {
  List<Product> products = [];
  Coupon? coupon;
  int? sum;
  String? error;
  Product? productInList;
  double subtotal = 0;
  double total = 0.0;
  double costDelivery = 0.0;
  double descuentoPorCupon = 0.0;
  double packageDiscount = 0.0;
  int compraMinimaEnvioGratis = 500;

  final repository = Repository();

  init() async {
    await fetchProducts();
  }

  fetchProducts() async {
    var products = await repository.getProducts();
    products = products["result"];
    this.products = (products as List).map(
      (e) {
        var product = Product.fromJson(e);
        product.selected = 0;
        return product;
      },
    ).toList();
    notifyListeners();
  }

  addProduct(Product product) {
    productInList = products.firstWhere(
      (element) => element.id == product.id,
    );
    var count = products.where((element) => element.selected == 1);
    sum = count.length;
    productInList?.selected = 1;
    productInList?.quantity = (productInList?.quantity ?? 0) + 1;
    notifyListeners();
  }

  removeProduct(Product product) {
    product.quantity = product.quantity! - 1;
    var count = products.where((element) => element.selected == 1);
    sum = count.length;
    notifyListeners();
  }

  checkPrices() {
    for (var i = 0; i < products.length; i++) {
      products[i].selected = 0;
      products[i].quantity = 0;
    }
  }

  calculateTotal() {
    var map = <String, dynamic>{};
    map = coupon!.payload!;
    int value = map['value'];
    int minimum = map['minimum'];
    double subTotalProduct = 0;
    int precio = 0;
    double costoDelivery = 50.0;
    List<int> addedProducts = [];

    Set<int> allMatches = {};
    products.map((e) {
      if (e.selected == 1) {
        addedProducts.add(e.id!);
        if (e.match != null) {
          allMatches.addAll({...e.match!});
        }
      }
    });

    for (var element in products) {
      if (element.selected == 1) {
        if (element.promotion == true && element.quantity! > 2) {
          precio = element.price!;
        }
        if (element.promotion != true) {
          if (allMatches.contains(element.id) ||
              element.match?.firstWhere(
                      (element1) => addedProducts.contains(element1),
                      orElse: () => -1) !=
                  -1) {
            packageDiscount += (element.price! * 0.1);
          }
        }
        subTotalProduct = ((element.price! * element.quantity!) - precio) * 1.0;
        subtotal += subTotalProduct;
      }
    }
    costDelivery = subtotal >= 500 ? 0 : costoDelivery;

    if (coupon!.code == 'PORCENTAJE' && subtotal > minimum) {
      descuentoPorCupon = subtotal * (value / 100);
    } else if (coupon!.code == 'FIJO') {
      descuentoPorCupon = value * 1.0;
    } else {
      descuentoPorCupon = 0.0;
    }
    total = subtotal - descuentoPorCupon + costDelivery - packageDiscount;
    notifyListeners();
  }

  getCoupon(String code) async {
    error = null;
    var cupon = await repository.getCoupon(code);
    cupon = cupon["result"];
    if (cupon != null) {
      coupon = Coupon.fromJson(cupon);
      notifyListeners();
    } else {
      error = "El cupón no existe";
      notifyListeners();
    }
  }

  clearCart() {
    for (var i = 0; i < products.length; i++) {
      products[i].selected = 0;
      products[i].quantity = 0;
    }
    total = 0.0;
    subtotal = 0;
    descuentoPorCupon = 0.0;
    costDelivery = 0;
    packageDiscount = 0.0;

    notifyListeners();
  }

  pay(BuildContext context) {
    clearCart();
    coupon = null;
    Navigator.of(context).pop();
  }
}

import 'package:flutter/material.dart';
import 'package:separate_api/providers/multi_provider.dart';
import 'package:separate_api/routes/routes.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProviderWidget(
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        initialRoute: 'home',
        routes: appRoutes,
      ),
    );
  }
}
